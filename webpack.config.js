var path = require("path");
module.exports = {
	context: path.join(__dirname , "app"),
	entry:{
		app:[
			/*"webpack/hot/dev-server",*/
			"./index.js"
		]
	},
	output: {
		path: path.join(__dirname, "app"),
		filename: "bundle.js"
	},
	module: {
		loaders:[
			{test:/\.js$/, loader:"babel", exclude:/node_modules/},
			{test:/\.html$/, loader:"raw", exclude:/node_modules/}
		]
	}
};
