module.exports = (app) => {
    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state("root", {
                url: "/test",
                template: require("./modules/common/views/test.html"),
                controller: "TestCtrl",
                controllerAs: "tc"
            })
            .state("login", {
                url: "/login",
                template: require("./modules/common/views/login.html"),
                controller: "LoginCtrl",
                controllerAs: "tc"
            })
    }]);
}
