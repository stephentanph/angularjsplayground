class LoginCtrl {
    constructor() {
        this.testVariable = "abc";
    }
    login() {
        this.testVariable = "eee";
    }
}
LoginCtrl.$inject = ["$scope"];

module.exports = (app) => {
    app.controller("LoginCtrl", LoginCtrl);
}
