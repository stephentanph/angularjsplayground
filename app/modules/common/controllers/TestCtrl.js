class TestCtrl {
    constructor() {
        this.testVariable = "button not clicked";
    }
    testClick() {
        this.testVariable = "BUTTON CLICKED";
    }
}
TestCtrl.$inject = ["$scope"];

module.exports = (app) => {
    app.controller("TestCtrl", TestCtrl);
}
