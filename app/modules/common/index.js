module.exports = function(app) {
    // load all controllers for this module
    require("./controllers/TestCtrl.js")(app);
    require("./controllers/LoginCtrl.js")(app);
}
