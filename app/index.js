import angular from "angular";
import "angular-ui-router";

const app = angular.module("app", [
	// dep injection for external library
    "ui.router"
]);

// add common module ( initializes all controllers )
require("./modules/common")(app);

// initialize app routing
require("./routes")(app);
